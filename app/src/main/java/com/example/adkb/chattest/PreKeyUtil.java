package com.example.adkb.chattest;

import android.content.Context;
import android.util.Log;

import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.InvalidKeyIdException;
import org.whispersystems.libsignal.ecc.Curve;
import org.whispersystems.libsignal.ecc.ECKeyPair;
import org.whispersystems.libsignal.state.PreKeyRecord;
import org.whispersystems.libsignal.state.PreKeyStore;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.state.SignedPreKeyStore;
import org.whispersystems.libsignal.util.Medium;

import java.util.LinkedList;
import java.util.List;

public class PreKeyUtil {
    private static final String TAG = PreKeyUtil.class.getName();
    private static final int BATCH_SIZE = 100;

    public static List<PreKeyRecord> generatePreKeys(PreKeyStore store) {
        List<PreKeyRecord> records = new LinkedList<>();
        int preKeyIdOffset = 0;

        for (int i = 0; i < BATCH_SIZE; ++i) {
            int preKeyId = (preKeyIdOffset + i) % Medium.MAX_VALUE;
            ECKeyPair keyPair = Curve.generateKeyPair();
            PreKeyRecord record = new PreKeyRecord(preKeyId, keyPair);
            store.storePreKey(preKeyId, record);
            records.add(record);
        }

        return records;
    }

    public static PreKeyRecord generateLastResortKey(PreKeyStore store) {
        if (store.containsPreKey(Medium.MAX_VALUE)) {
            try {
                return store.loadPreKey(Medium.MAX_VALUE);
            } catch (InvalidKeyIdException e) {
                Log.w("PreKeyUtil", e);
                store.removePreKey(Medium.MAX_VALUE);
            }
        }

        ECKeyPair keyPair = Curve.generateKeyPair();
        PreKeyRecord record = new PreKeyRecord(Medium.MAX_VALUE, keyPair);
        store.storePreKey(Medium.MAX_VALUE, record);

        return record;
    }

    public static SignedPreKeyRecord generateSignedPreKey(SignedPreKeyStore store,
                                                          IdentityKeyPair identityKeyPair,
                                                          boolean active)
    {
        try {
            int signedPreKeyId = 0;
            ECKeyPair keyPair = Curve.generateKeyPair();
            byte[] signature = Curve.calculateSignature(identityKeyPair.getPrivateKey(), keyPair.getPublicKey().serialize());
            SignedPreKeyRecord record = new SignedPreKeyRecord(signedPreKeyId, System.currentTimeMillis(), keyPair, signature);

            store.storeSignedPreKey(signedPreKeyId, record);
            return record;
        } catch (InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }
}
