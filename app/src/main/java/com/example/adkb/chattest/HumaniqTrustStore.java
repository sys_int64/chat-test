package com.example.adkb.chattest;

import android.content.Context;
import org.whispersystems.signalservice.api.push.TrustStore;
import java.io.InputStream;

public class HumaniqTrustStore implements TrustStore {
    private final Context context;

    HumaniqTrustStore(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    public InputStream getKeyStoreInputStream() {
        return context.getResources().openRawResource(R.raw.humaniq_lets_encrypt_x3_cross_signed);
    }

    @Override
    public String getKeyStorePassword() {
        return "humaniq";
    }
}
