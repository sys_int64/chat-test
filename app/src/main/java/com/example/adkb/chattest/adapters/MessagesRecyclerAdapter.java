package com.example.adkb.chattest.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adkb.chattest.R;
import com.example.adkb.chattest.ViewContext;
import com.example.adkb.chattest.holders.MessageItemHolder;
import com.example.adkb.chattest.models.Message;

import java.util.List;

public class MessagesRecyclerAdapter extends ItemRecyclerAdapter<Message> {
    public MessagesRecyclerAdapter(ViewContext viewContext, List<Message> items, HolderFactory holderFactory)
    {
        super(viewContext, R.layout.recycler_item_message_inbox, items, holderFactory);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case Message.ViewType.INBOX:
                view = inflater.inflate(R.layout.recycler_item_message_inbox, parent, false);
                return new MessageItemHolder(getViewContext(), view);

            case Message.ViewType.OUTBOX:
                view = inflater.inflate(R.layout.recycler_item_message_outbox, parent, false);
                return new MessageItemHolder(getViewContext(), view);

            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }
}
