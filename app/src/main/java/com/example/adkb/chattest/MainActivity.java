package com.example.adkb.chattest;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.annotation.IdRes;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.adkb.chattest.adapters.ItemRecyclerAdapter;
import com.example.adkb.chattest.adapters.MessagesRecyclerAdapter;
import com.example.adkb.chattest.holders.MessageItemHolder;
import com.example.adkb.chattest.models.Message;
import com.example.adkb.chattest.service.MessageRetrievalService;
import com.example.adkb.chattest.service.User1MessageRetrievalService;
import com.example.adkb.chattest.service.User2MessageRetrievalService;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@SuppressLint("StaticFieldLeak")
public class MainActivity extends AppCompatActivity implements View.OnClickListener, ViewContext {
    private static final String TAG = "MainActivity";
    public static SignalServiceAccountManager accountManager;

    static private AccountManager user1Manager;
    static private AccountManager user2Manager;
    private RadioButton radioUser1;
    private RadioButton radioUser2;
    private EditText textViewMessage;

    private List<Message> messages = new ArrayList<>();
    private ItemRecyclerAdapter<Message> recyclerAdapter;
    private RecyclerView recyclerMessages;
    private LinearLayoutManager recyclerLayoutManager;

    public static AccountManager getUser1Manager() {
        return user1Manager;
    }

    public static AccountManager getUser2Manager() {
        return user2Manager;
    }

    void attachOnClick(@IdRes int viewId) {
        View view = this.findViewById(viewId);
        view.setOnClickListener(this);
    }

    void initMock() {
        messages.add(new Message(Message.ViewType.OUTBOX, user1Manager, user2Manager, "Hi there"));
        messages.add(new Message(Message.ViewType.INBOX, user2Manager, user1Manager, "Hi"));
        messages.add(new Message(Message.ViewType.OUTBOX, user1Manager, user2Manager, "How Are you?"));
        messages.add(new Message(Message.ViewType.INBOX, user2Manager, user1Manager, "Fine, and you?"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        attachOnClick(R.id.buttonSend);

        radioUser1 = (RadioButton) findViewById(R.id.radioUser1);
        radioUser2 = (RadioButton) findViewById(R.id.radioUser2);
        recyclerMessages = (RecyclerView) findViewById(R.id.recyclerMessages);
        textViewMessage = (EditText) findViewById(R.id.textViewMessage);
        textViewMessage.setImeActionLabel("SEND", KeyEvent.KEYCODE_ENTER);

        textViewMessage.setOnEditorActionListener((textView, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                sendMessage();
                return true;
            }
            return false;

        });

        user1Manager = new AccountManager(this, "+79137698361", "123321");
        user2Manager = new AccountManager(this, "+79137698362", "123321");

        AsyncTask<Object, Object, Object> task = new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object... objects) {
                try {
                    user1Manager.register();
                    user2Manager.register();

                    user1Manager.setGcmId(MainActivity.this);
                    user2Manager.setGcmId(MainActivity.this);

                    user1Manager.addIdentity(user2Manager);
                    user2Manager.addIdentity(user1Manager);
//                    user2Manager.setGcmId(Optional.of(GoogleCloudMessaging.getInstance(activity).register("919291061359")));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        task.execute();

        initRecycler();
        initMock();
    }

    void sendMessage() {
        AccountManager sender;
        AccountManager recipient;

//        sender = user1Manager;
//        recipient = user2Manager;
        int viewType;

        if (radioUser1.isChecked()) {
            sender = user1Manager;
            recipient = user2Manager;
//            viewType = Message.ViewType.OUTBOX;
        } else {
            sender = user2Manager;
            recipient = user1Manager;
//            viewType = Message.ViewType.INBOX;
        }

        final String textMessage = textViewMessage.getText().toString();

        AsyncTask<Object, Object, Object> task = new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object... objects) {
                try {
                    sender.sendMessage(textMessage, recipient);
                    Log.d(TAG, "Sent message from " + sender.getUserNumber() + " to " + recipient.getUserNumber() + " with text: '" + textMessage + "'");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (UntrustedIdentityException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                textViewMessage.setText("");
            }
        };

        task.execute();
    }

    void putMessage(String sender, String textMessage) {
        final int viewType = sender.equals(user1Manager.getUserNumber())
            ? Message.ViewType.OUTBOX
            : Message.ViewType.INBOX;

        final Message message = new Message(viewType, sender, "", textMessage);

        textViewMessage.setText("");
        messages.add(message);
        final int position = messages.indexOf(message);
        recyclerAdapter.notifyItemInserted(position);
        recyclerLayoutManager.scrollToPosition(position);
    }

    public void onMessageReceived(Intent intent) {
        final String source = intent.getStringExtra("source");
        final String message = intent.getStringExtra("message");

        Log.d(TAG, source + ": " + message);
        putMessage(source, message);
    }

    void initRecycler() {
        recyclerLayoutManager = new LinearLayoutManager(this);
        recyclerLayoutManager.setStackFromEnd(true);
        recyclerLayoutManager.scrollToPosition(0);
        recyclerAdapter = new MessagesRecyclerAdapter(this,
            messages,
            MessageItemHolder::new);

        recyclerMessages.setLayoutManager(recyclerLayoutManager);
        recyclerMessages.setAdapter(recyclerAdapter);
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onMessageReceived(intent);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSend:
                sendMessage();
                break;
        }
    }

    @Override
    public Context getInstance() {
        return this;
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
            new IntentFilter("libsignals-message"));

        Intent intent = new Intent(this, User1MessageRetrievalService.class);
        Intent intent2 = new Intent(this, User2MessageRetrievalService.class);

        this.startService(intent);
        this.startService(intent2);
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);

        Intent intent = new Intent(this, User1MessageRetrievalService.class);
        Intent intent2 = new Intent(this, User2MessageRetrievalService.class);

        this.stopService(intent);
        this.stopService(intent2);
    }
}
