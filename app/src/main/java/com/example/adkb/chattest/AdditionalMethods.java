package com.example.adkb.chattest;

import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.conn.ssl.StrictHostnameVerifier;
import org.whispersystems.signalservice.internal.push.AccountAttributes;
import org.whispersystems.signalservice.internal.util.BlacklistingTrustManager;
import org.whispersystems.signalservice.internal.util.JsonUtil;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

public class AdditionalMethods {
    private static final String TAG = "AdditionalMethods";

    static boolean registration(AccountManager accountManager)
        throws NoSuchAlgorithmException, KeyManagementException, IOException
    {
        TrustManager[] trustManagers = BlacklistingTrustManager.createFor(accountManager.getUrl().getTrustStore());
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustManagers, null);

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setSslSocketFactory(sslContext.getSocketFactory());
        okHttpClient.setHostnameVerifier(new StrictHostnameVerifier());

        Log.e(TAG, accountManager.getSignalingKey());
        Log.e(TAG, String.valueOf(accountManager.getRegistrationId()));

        final AccountAttributes signalingKeyEntity = new AccountAttributes(
            accountManager.getSignalingKey(),
            accountManager.getRegistrationId(),
            false
        );

        final String body = JsonUtil.toJson(signalingKeyEntity);
        Log.e(TAG, body);

        Request.Builder request = new Request.Builder();
        request.url(String.format("%s%s", accountManager.getUrl().getUrl(), "/v1/accounts/registration/"));
        request.method("POST", RequestBody.create(MediaType.parse("application/json"), body));

        request.addHeader("Authorization", accountManager.getAuthHeader());
        request.addHeader("X-Signal-Agent", Config.USER_AGENT);

        Response response = okHttpClient.newCall(request.build()).execute();
        Log.e(TAG, response.message());

        return response.isSuccessful();
    }
}
