package com.example.adkb.chattest;

import org.whispersystems.libsignal.util.KeyHelper;

public class Config {
    static public String signalingKey = Util.getSecret(52);
    static public int registrationId = KeyHelper.generateRegistrationId(false);
    static public String LOCAL_NUMBER = "+79137698347";
    static public String LOCAL_PASSWORD = "123321";
    static public String USER_AGENT = "TA";
    static public String URL = "https://beta-api.humaniq.co/signal";
}
