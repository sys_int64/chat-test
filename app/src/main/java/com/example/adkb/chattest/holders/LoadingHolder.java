package com.example.adkb.chattest.holders;

import android.view.View;

import com.example.adkb.chattest.ViewContext;
import com.example.adkb.chattest.models.BaseModel;

public class LoadingHolder extends RecyclerItemHolder<BaseModel> {
    public LoadingHolder(ViewContext viewContext, View itemView) {
        super(viewContext, itemView);
    }
}
