package com.example.adkb.chattest.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.adkb.chattest.AccountManager;

import org.whispersystems.libsignal.DuplicateMessageException;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.InvalidKeyIdException;
import org.whispersystems.libsignal.InvalidMessageException;
import org.whispersystems.libsignal.InvalidVersionException;
import org.whispersystems.libsignal.LegacyMessageException;
import org.whispersystems.libsignal.NoSessionException;
import org.whispersystems.libsignal.UntrustedIdentityException;
import org.whispersystems.signalservice.api.SignalServiceMessagePipe;
import org.whispersystems.signalservice.api.crypto.SignalServiceCipher;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageRetrievalService extends Service {
    private static final String TAG = "MessageRetrieval";
    private MessageRetrievalThread retrievalThread  = null;

    protected AccountManager getAccountManager() {
        throw new UnsupportedOperationException();
    }

    private void sendMessage(String source, String message) {
        Intent intent = new Intent("libsignals-message");
        intent.putExtra("message", message);
        intent.putExtra("source", source);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        retrievalThread = new MessageRetrievalThread();
        retrievalThread.start();
    }

    private class MessageRetrievalThread extends Thread implements Thread.UncaughtExceptionHandler {
        private AtomicBoolean stopThread = new AtomicBoolean(false);

        MessageRetrievalThread() {
            setUncaughtExceptionHandler(this);
        }

        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {
            Log.w(TAG, "*** Uncaught exception!");
            Log.w(TAG, throwable);
        }

        @Override
        public void run() {
            while (!stopThread.get()) {
                if (!getAccountManager().isReady())
                    continue;

                Log.d(TAG, getAccountManager().getUserNumber() + " is ready");
//                sendMessage(getAccountManager().getUserNumber(), "Hello");

                SignalServiceMessagePipe pipe = getAccountManager()
                    .getMessageReceiver().createMessagePipe();

                try {
                    handle(pipe);
                } catch (TimeoutException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvalidVersionException e) {
                    e.printStackTrace();
                } finally {
                    shutdown(pipe);
                    Log.d(TAG, getAccountManager().getUserNumber() + ": shutdown pipe");
                }

                Log.d(TAG, getAccountManager().getUserNumber() + ": Loop");
            }

            Log.d(TAG, getAccountManager().getUserNumber() + ": End thread");
        }

        private void handle(SignalServiceMessagePipe pipe) throws InvalidVersionException, IOException, TimeoutException {
            pipe.read(1, TimeUnit.MINUTES,
                envelope -> {
                    Log.w(TAG, "Retrieved envelope! " + envelope.getSource());
                    Log.w(TAG, "My address is " + getAccountManager().getUserNumber());
                    final SignalServiceCipher cipher = new SignalServiceCipher(
                        new SignalServiceAddress(getAccountManager().getUserNumber()),
                        getAccountManager().getProtocolStore());
                    final SignalServiceContent content;

                    try {
                        content = cipher.decrypt(envelope);

                        if (!content.getDataMessage().isPresent()) {
                            Log.d(TAG, "data is not presented");
                            return;
                        }

                        Log.d(TAG, "message decrypted");
                        Log.d(TAG, "Sync message: " + content.getSyncMessage().toString());
                        Log.d(TAG, "Data message: " + content.getDataMessage().toString());
                        final String message = content.getDataMessage().get().getBody().get();
                        Log.d(TAG, "Text message: " + message);
                        sendMessage(envelope.getSource(), message);
                    } catch (InvalidVersionException e) {
                        e.printStackTrace();
                    } catch (InvalidMessageException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (DuplicateMessageException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyIdException e) {
                        e.printStackTrace();
                    } catch (UntrustedIdentityException e) {
                        e.printStackTrace();
                    } catch (LegacyMessageException e) {
                        e.printStackTrace();
                    } catch (NoSessionException e) {
                        e.printStackTrace();
                    }
                });
        }
    }

    private void shutdown(SignalServiceMessagePipe pipe) {
        try {
            pipe.shutdown();
        } catch (Throwable t) {
            Log.w(TAG, t);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
