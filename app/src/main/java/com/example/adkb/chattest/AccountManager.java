package com.example.adkb.chattest;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.example.adkb.chattest.store.DebugSignalProtocolStore;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.state.PreKeyRecord;
import org.whispersystems.libsignal.state.SignalProtocolStore;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.state.impl.InMemorySignalProtocolStore;
import org.whispersystems.libsignal.util.KeyHelper;
import org.whispersystems.libsignal.util.Medium;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.TrustStore;
import org.whispersystems.signalservice.internal.push.SignalServiceUrl;
import org.whispersystems.signalservice.internal.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class AccountManager {
    private static final String TAG = "AccountManager";
    final private String userNumber;
    final private String userPassword;
    final private Context context;
    final private TrustStore trustStore;
    final private String signalingKey;
    final private int registrationId;
    final private SignalServiceUrl url;
    private boolean ready = false;

    private SignalServiceAccountManager signalServiceAccountManager;
    private SignalProtocolStore protocolStore;
    private SignalServiceMessageReceiver messageReceiver;
    private SignalServiceMessageSender messageSender;

    private IdentityKeyPair identityKeyPair;
    private List<PreKeyRecord> oneTimePreKeys;
    private PreKeyRecord lastResortKey;
    private SignedPreKeyRecord signedPreKeyRecord;

    AccountManager(Context context, String userNumber, String userPassword) {
        this.context = context;
        this.userNumber = userNumber;
        this.userPassword = userPassword;
        this.trustStore = new HumaniqTrustStore(context);
        this.signalingKey = Util.getSecret(52);
        this.registrationId = KeyHelper.generateRegistrationId(false);
        this.url = new SignalServiceUrl(Config.URL, this.trustStore);
    }

    void register() throws NoSuchAlgorithmException, KeyManagementException,
        IOException, InvalidKeyException {
        signalServiceAccountManager = new SignalServiceAccountManager(
            new SignalServiceUrl[]{this.url},
            this.userNumber,
            this.userPassword,
            Config.USER_AGENT
        );

        if (AdditionalMethods.registration(this)) {
            init();
            ready = true;
        } else {
            Log.e(TAG, "Not registered");
        }
    }

    public void addIdentity(AccountManager accountManager) {
        protocolStore.saveIdentity(new SignalProtocolAddress(accountManager.getUserNumber(), 0), accountManager.identityKeyPair.getPublicKey());
    }

    private void init() throws InvalidKeyException, IOException {
//        this.identityKeyPair = KeyHelper.generateIdentityKeyPair();
//        this.oneTimePreKeys = KeyHelper.generatePreKeys(0, 100);
//        this.lastResortKey = KeyHelper.generateLastResortPreKey();
//        this.signedPreKeyRecord = KeyHelper.generateSignedPreKey(identityKeyPair, signalPreKeyId);

        this.identityKeyPair = KeyHelper.generateIdentityKeyPair();

        protocolStore = new DebugSignalProtocolStore(this.identityKeyPair, this.registrationId);

        this.oneTimePreKeys = PreKeyUtil.generatePreKeys(protocolStore);
        this.lastResortKey = PreKeyUtil.generateLastResortKey(protocolStore);
        this.signedPreKeyRecord = PreKeyUtil.generateSignedPreKey(protocolStore, this.identityKeyPair, true);

//        // Store PreKeys
//        for (PreKeyRecord key : oneTimePreKeys) {
//            int id = (signalPreKeyId + i) % Medium.MAX_VALUE;
//            ++i;
//            Log.d(TAG, String.valueOf(id));
//            protocolStore.storePreKey(id, key);
//        }
//
//        // Store LastResortKey
//        protocolStore.storePreKey(Medium.MAX_VALUE, lastResortKey);
//
//        // Store SignedPreKey
//        protocolStore.storeSignedPreKey(signalPreKeyId, signedPreKeyRecord);

        signalServiceAccountManager.setPreKeys(
            this.identityKeyPair.getPublicKey(),
            lastResortKey,
            signedPreKeyRecord,
            oneTimePreKeys
        );

        initConnectionService();
    }

    private void initConnectionService() {
        messageSender = new SignalServiceMessageSender(
            new SignalServiceUrl[]{this.url},
            this.userNumber,
            this.userPassword,
            this.protocolStore,
            Config.USER_AGENT,
            Optional.<SignalServiceMessageSender.EventListener>absent()
        );

        messageReceiver = new SignalServiceMessageReceiver(
            new SignalServiceUrl[]{this.url},
            this.userNumber,
            this.userPassword,
            this.signalingKey,
            Config.USER_AGENT
        );
    }

    void sendMessage(String message, AccountManager recipient) throws IOException,
        UntrustedIdentityException
    {
        SignalServiceDataMessage.Builder builder = SignalServiceDataMessage.newBuilder();
        builder.withBody(message);
        SignalServiceDataMessage dataMessage = builder.build();

        messageSender.sendMessage(
            new SignalServiceAddress(recipient.getUserNumber()),
            builder.build()
        );
    }

    String getAuthHeader() {
        try {
            return "Basic " +
                Base64.encodeBytes(
                    (this.userNumber + ":" + this.userPassword).getBytes("UTF-8")
                );
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    String getSignalingKey() {
        return signalingKey;
    }

    int getRegistrationId() {
        return registrationId;
    }

    SignalServiceUrl getUrl() {
        return url;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public SignalServiceMessageReceiver getMessageReceiver() {
        return messageReceiver;
    }

    public SignalProtocolStore getProtocolStore() {
        return protocolStore;
    }

    public boolean isReady() {
        return ready;
    }

    public void setGcmId(Activity activity) {
        try {
            signalServiceAccountManager.setGcmId(Optional.of(GoogleCloudMessaging.getInstance(activity).register("919291061359")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
