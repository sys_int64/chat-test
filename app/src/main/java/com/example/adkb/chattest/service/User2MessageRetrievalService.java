package com.example.adkb.chattest.service;

import com.example.adkb.chattest.AccountManager;
import com.example.adkb.chattest.MainActivity;

public class User2MessageRetrievalService extends MessageRetrievalService {
    @Override
    protected AccountManager getAccountManager() {
        return MainActivity.getUser2Manager();
    }
}
