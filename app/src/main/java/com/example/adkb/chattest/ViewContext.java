package com.example.adkb.chattest;

import android.content.Context;

public interface ViewContext {
    Context getInstance();
}
