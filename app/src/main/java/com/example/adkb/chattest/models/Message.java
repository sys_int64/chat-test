package com.example.adkb.chattest.models;

import com.example.adkb.chattest.AccountManager;

public class Message implements BaseModel {
    public class ViewType {
        static public final int INBOX = 0;
        static public final int OUTBOX = 1;
    }

    private String from;
    private String to;
    private String message;
    private int viewType;

    public Message(int viewType, String from, String to, String message) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.viewType = viewType;
    }

    public Message(int viewType, AccountManager from, AccountManager to, String message) {
        this.from = from.getUserNumber();
        this.to = to.getUserNumber();
        this.message = message;
        this.viewType = viewType;
    }

    @Override
    public int getViewType() {
        return viewType;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }
}
