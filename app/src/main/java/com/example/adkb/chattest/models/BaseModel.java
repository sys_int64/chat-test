package com.example.adkb.chattest.models;

public interface BaseModel {
    class ViewType {
        static public final int DATA = 999;
        static public final int LOADING = 998;
    }

    int getViewType();
}
