package com.example.adkb.chattest.holders;

import android.view.View;
import android.widget.TextView;

import com.example.adkb.chattest.R;
import com.example.adkb.chattest.ViewContext;
import com.example.adkb.chattest.models.Message;

public class MessageItemHolder extends RecyclerItemHolder<Message> {
    final private TextView messageView;
    final private TextView accountNameView;

    public MessageItemHolder(ViewContext viewContext, View itemView) {
        super(viewContext, itemView);

        messageView = (TextView) itemView.findViewById(R.id.textViewMessage);
        accountNameView = (TextView) itemView.findViewById(R.id.textViewAccountName);
    }

    @Override
    public void initViews(Message data) {
        messageView.setText(data.getMessage());
        accountNameView.setText(data.getFrom());
    }
}
